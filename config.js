const PORT = process.env.PORT || 8080;
const JWT_SECRET = process.env.JWT_SECRET || 'anothersecret';

module.exports = {PORT, JWT_SECRET};
