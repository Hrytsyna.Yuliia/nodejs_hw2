const mongoose = require('mongoose');

const NoteSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  authorName: {
    type: String,
    required: true,
    uniq: true,
  },
  authorId: {
    type: String,
    required: true,
    uniq: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Note = mongoose.model('Note', NoteSchema);
