const express = require('express');
const router = new express.Router();
const {User} = require('../models/userModel');
const {authMiddleware} = require('./middlewares/authMiddleware');
const bcrypt = require('bcrypt');

router.get('/', authMiddleware, async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if(!user) {
    res.status(400).json({message: `User with username ${req.user.username} not found!`});
  }

  res.status(200).json({message: 'Success', user: user});
});

router.patch('/', authMiddleware, async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  if (!oldPassword || !newPassword) {
    res.status(400).json({message: `Please specify all parameters!`});
  };

  const newPass = await bcrypt.hash(newPassword, 10);
  await User.findByIdAndUpdate(req.user._id, {password: newPass});
  res.status(200).json({message: `Password updates! Please log in!`});

});

router.delete('/', authMiddleware, async (req, res) => {
  
  if (!(await User.findByIdAndDelete(req.user._id))) {
    res.status(400).json({message: `User not found!`});
  }

  res.status(200).json({message: `User deleted!`});
});

module.exports = router;
