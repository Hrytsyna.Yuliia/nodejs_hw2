const express = require('express');
const router = new express.Router();
const {Note} = require('../models/noteModel');
const {authMiddleware} = require('./middlewares/authMiddleware');

router.get('/', authMiddleware, async (req, res) => {
  const {skip=0, limit=5} = req.query;
  const requestOptions = {
    skip: parseInt(skip),
    limit: limit > 100 ? 5 : parseInt(limit),
  };
  const notes = await Note
      .find({authorId: req.user._id})
      .skip(requestOptions.skip)
      .limit(requestOptions.limit);

  if(!notes) {
    res.status(400).json({message: `You don't have any notes!`});
  }
  res.status(200).json({notes});
});

router.post('/', authMiddleware, async (req, res) => {
  if (req.body.text) {
    const note = new Note({
      text: req.body.text,
      authorName: req.user.username,
      authorId: req.user._id,
      completed: false,
    });
    await note.save();
    res.status(200).json({message: 'New note added!'});
  }

  res.status(400).json({message: 'Please specify all parameters!'});
});

router.get('/:id', authMiddleware, async (req, res) => {
  const note = await Note.findOne({_id: req.params.id});
  
  if (!note) {
    res.status(400).json({message: 'Nothing found!'});
  }
  
  res.status(200).json({note: note});
});

router.put('/:id', authMiddleware, async (req, res) => {
  if (!req.body.text) {
    res.status(400).json({message: 'Please specify all parameters!'});
  }
  try {
    await Note.findByIdAndUpdate(req.params.id, {text: req.body.text});
    res.status(200).json({message: 'Note updated'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.patch('/:id', authMiddleware, async (req, res) => {
  let note = await Note.findOne({_id: req.params.id});
  if(!note) {
    res.status(400).json({message: 'Nothing found!'});
  }
  try {
    if (note.completed) {
      await Note.findByIdAndUpdate(req.params.id, {completed: false});
      console.log(typeof note.completed);
    } else {
      await Note.findByIdAndUpdate(req.params.id, {completed: true});
      console.log(typeof note.completed);
    }
    res.status(200).json({message: 'Status changed'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/:id', authMiddleware, async (req, res) => {
  try {
    await Note.findByIdAndDelete(req.params.id);
    res.status(200).json({message: 'Note deleted'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = router;

// http://localhost:8080/api/notes/602f8e6e09136908f429b7db