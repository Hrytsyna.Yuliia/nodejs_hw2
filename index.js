const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const {PORT} = require('./config');

app.use(express.json());
app.use(morgan('tiny'));

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const notesRouter = require('./routers/notesRouter');

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  await mongoose.connect('mongodb+srv://yuliia:yuliia@cluster0.bryei.mongodb.net/hw2_db?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  console.log('db connected');
  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
};

start();
